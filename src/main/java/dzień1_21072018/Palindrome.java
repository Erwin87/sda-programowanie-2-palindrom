package dzień1_21072018;

import java.security.InvalidParameterException;

public class Palindrome {

    public static void main(String[] args) {
        String text1 = "Ala ma kota";
        String text2 = "kobyłamamałybok";

        System.out.println(isPalindrome(text1));
        System.out.println(isPalindrome(text2));
    }

    static boolean isPalindrome(String text) {
        if(text == null){
            throw new InvalidParameterException("Parametr jest null");
        }

        int lewy = 0;
        int prawy = text.length() - 1;

        while (lewy < prawy) {
            if (text.charAt(lewy) != text.charAt(prawy)) {
                return false;
            }
            lewy++;
            prawy--;
        }

        return true;

    }
}
