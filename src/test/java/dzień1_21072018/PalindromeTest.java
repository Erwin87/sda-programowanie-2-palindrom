package dzień1_21072018;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import org.junit.rules.ExpectedException;

import java.security.InvalidParameterException;


public class PalindromeTest {

    private Palindrome palindrome;

    String text1 = "Ala ma kota";
    String text2 = "ala";
    String text3 = null;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
        palindrome = new Palindrome();
    }

    @Test
    public void shouldBeTrueIfPalindorme(){
        assertTrue(palindrome.isPalindrome(text2));
    }

    @Test
    public void shouldBeFalseIfNotPalindorme(){
        assertFalse(palindrome.isPalindrome(text1));
    }



    @Test
    public void shouldThrowExceptionIfNull(){
        //assertNull(palindrome.isPalindrome(text3));

        expectedException.expect(InvalidParameterException.class);
        expectedException.expectMessage("Parametr jest null");
        palindrome.isPalindrome(text3);
    }



}
